<?php

/**
 * @file
 * Admin page
 */

/**
 * Create page with information about setting of Google reCAPTCHA
 * Is keys were input?
 * List of all protected forms
 * Statistics data - if was enabled (default)
 * Some module settings
 * Where to get additional information about Google reCAPTCHA
 *
 * @return string
 */
function g_captcha_overview() {
  $conf = variable_get('g_captcha', []);
  $output = theme_render_template(
    drupal_get_path('module', 'g_captcha') . '/g-captcha-admin.tpl.php', $conf
  );
  return $output;
}

/**
 * @file
 * Admin page
 */

/**
 * Central handler for CAPTCHA point administration (adding, disabling, deleting).
 */
function g_captcha_instance_admin($form_id = NULL, $op = NULL) {

  // If $captcha_point_form_id and action $op given: do the action.
  if ($form_id) {
    switch ($op) {
      case 'delete':
        return drupal_get_form('g_captcha_instance_disable_confirm', $form_id, TRUE);
    }
  }
  // Return add form for CAPTCHA point.
  return drupal_get_form('g_captcha_forms_form');
}


/**
 * Confirm dialog for disabling/deleting a CAPTCHA point.
 */
function g_captcha_instance_disable_confirm($form, &$form_state, $form_id, $delete) {
  $form = array();
  $form['instance_id'] = array(
    '#type' => 'value',
    '#value' => $form_id,
  );
  $form['g_captcha_delete'] = array(
    '#type' => 'value',
    '#value' => $delete,
  );
  if ($delete) {
    $message = t('Are you sure you want to delete the CAPTCHA for form_id %form_id?', array('%form_id' => $form_id));
    $yes = t('Delete');
  }
  return confirm_form($form, $message, 'admin/config/spam_protection/g_captcha/forms', '', $yes);
}

/**
 * Submission handler of CAPTCHA point disabling/deleting confirm_form.
 */
function g_captcha_instance_disable_confirm_submit($form, &$form_state) {
  $form_id = $form_state['values']['instance_id'];
  $delete = $form_state['values']['g_captcha_delete'];
  if ($delete) {
    db_delete('g_captcha_instances')
    ->condition('form_id', $form_id)
    ->execute();
    drupal_set_message(t('Deleted CAPTCHA for form %form_id.', array('%form_id' => $form_id)));
  }
  $form_state['redirect'] = 'admin/config/spam_protection/g_captcha/forms';
}
