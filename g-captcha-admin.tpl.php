<?php

/**
 * @file
 *  for "Summary" admin page.
 *
 * Available variables
 * - $public_key: Google reCAPTCHA public key
 * - $secret_key: Google reCAPTCHA secret key
 * - $statistics['status']: Is statistics collects or not
 * - $statistics['requests']: Count of all requests to Google servers
 * - $statistics['success']: Count correct filled reCAPTCHA
 * - $statistics['fails']: Count incorrect filled (or not filled) reCAPTCHA
 */
?>
<h2><?php print t('API Keys'); ?></h2>
<p><strong><?php print t('Public key'); ?>:</strong>
  <?php if (empty($public_key)): ?>
    <?php print t('
    It seems that You are not defined Google reCAPTCHA public key for this site
    yet. You can do it <a href="@page-with-keys">here >></a>',
      array('@page-with-keys' => '/admin/config/spam_protection/g_captcha/keys')); ?>
  <?php else: ?>
    <?php print $public_key; ?>
  <?php endif; ?>
  <br />
  <strong><?php print t('Secret key'); ?>:</strong>
  <?php if (empty($secret_key)): ?>
    <?php print t('
    It seems that You are not defined Google reCAPTCHA secret key for this site
    yet. You can do it <a href="@page-with-keys">here >></a>',
      array('@page-with-keys' => '/admin/config/spam_protection/google_recaptcha/keys')); ?>
  <?php else: ?>
    <?php print $secret_key; ?>
  <?php endif; ?>
</p>

<h2><?php print t('Widget Defaults'); ?></h2>
<p><strong><?php print t('Language'); ?>:</strong>
  <?php if(isset($settings['language'])) {
    $languages = g_captcha_language_code_options();
    print $languages[$settings['language']];
  } ?>
  <br />
  <strong><?php print t('Size'); ?>:</strong>
  <?php if(isset($settings['widget_size'])) {
    print ucfirst($settings['widget_size']);
  } ?>
  <br />
  <strong><?php print t('Theme'); ?>:</strong>
  <?php if(isset($settings['widget_theme'])) {
    print ucfirst($settings['widget_theme']);
  } ?>
</p>
