<?php

/**
 * @file
 * Admin page
 */

/**
 * Create form for managing list of protected forms.
 *
 * @param $form
 * @param $form_state
 *
 * @return array
 */
function g_captcha_forms_form($form, &$form_state) {
  $conf = variable_get('g_captcha');
  // Configuration of which forms to protect, with what challenge.
  $form['captcha_form_protection'] = array(
    '#type' => 'fieldset',
    '#title' => t('Form protection'),
    '#description' => t("Select the challenge type you want for each of the listed forms (identified by their so called <em>form_id</em>'s)."),
  );
  // List known form_ids.
  $form['captcha_form_protection']['captcha_form_id_overview'] = array(
    '#theme' => 'g_captcha_forms_instances',
    '#tree' => TRUE,
  );
  $form['captcha_form_protection']['captcha_form_id_overview']['captcha_captcha_points'] = array();

  $types = _g_captcha_types();
  $sizes = _g_captcha_sizes(TRUE);
  $themes = _g_captcha_themes(TRUE);
  $forms = g_captcha_get_instances();
  $instances = (array) $forms;
  foreach($instances as $instance) {

    $elem = array();
    $elem['form_id'] = array(
      '#markup' => $instance->form_id,
    );

    if (isset($instance->captcha_type)) {
      $captcha_type = $instance->captcha_type;
    }
    else {
      $captcha_type = 'default';
    }

    $captcha_settings = json_decode(
      implode(
        (array) $instance->captcha_settings
      )
    );

    if (isset($captcha_settings->size)) {
      $captcha_size = $captcha_settings->size;
    }
    else {
      $captcha_size = 'normal';
    }

    if (isset($captcha_settings->theme)) {
      $captcha_theme = $captcha_settings->theme;
    }
    else {
      $captcha_theme = 'default';
    }

    $elem['captcha_type'] = array(
      '#type' => 'select',
      '#default_value' => $captcha_type,
      '#options' => $types
    );
    $elem['captcha_size'] = array(
      '#type' => 'select',
      '#default_value' => $captcha_size,
      '#options' => $sizes
    );
    $elem['captcha_theme'] = array(
      '#type' => 'select',
      '#default_value' => $captcha_theme,
      '#options' => $themes
    );
    $ops = array();
      if ($instance->export_type & EXPORT_IN_DATABASE) {
        $ops[] = l(t('revert'), "admin/config/spam_protection/g_captcha/forms/form/{$instance->form_id}/delete");
      }
    $elem['operations'] = array('#markup' => implode(", ", $ops));

    $form['captcha_form_protection']['captcha_form_id_overview']['captcha_captcha_points'][$instance->form_id] = $elem;

  }
  // Form items for new form_id.
  $form['captcha_form_protection']['captcha_form_id_overview']['captcha_new_captcha_point'] = array();
  // Textfield for form_id.
  $form['captcha_form_protection']['captcha_form_id_overview']['captcha_new_captcha_point']['form_id'] = array(
    '#type' => 'textfield',
    '#size' => 16,
  );
  // Select widget for CAPTCHA type.
  $form['captcha_form_protection']['captcha_form_id_overview']['captcha_new_captcha_point']['captcha_type'] = array(
    '#type' => 'select',
    '#default_value' => 'default',
    '#options' => $types,
  );
  // Select widget for CAPTCHA type.
  $form['captcha_form_protection']['captcha_form_id_overview']['captcha_new_captcha_point']['captcha_size'] = array(
    '#type' => 'select',
    '#default_value' => 'default',
    '#options' => $sizes,
  );
  // Select widget for CAPTCHA type.
  $form['captcha_form_protection']['captcha_form_id_overview']['captcha_new_captcha_point']['captcha_theme'] = array(
    '#type' => 'select',
    '#default_value' => 'default',
    '#options' => $themes,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save')
  );

  $form['#tree'] = TRUE;

  return $form;
}

/**
 * Submit handler
 *
 * @param $form
 * @param $form_state
 */
function g_captcha_forms_form_submit($form, &$form_state) {
  $conf = variable_get('g_captcha');

  drupal_set_message('Settings were saved.');

  if(isset($form_state['values']['captcha_form_protection']['captcha_form_id_overview']['captcha_captcha_points'])) {
    // Load existing data.
    $instances = g_captcha_get_instances();
    $forms = $form_state['values']['captcha_form_protection']['captcha_form_id_overview']['captcha_captcha_points'];
    foreach ($forms as $form_id => $data) {
      // If this is an in-code captcha point and its settings are unchanged,
      // don't save to the database.
      if (module_exists('ctools') && isset($instances[$form_id])) {
        // Parse module and captcha_type from submitted values.
        if (is_string($data['captcha_type']) && substr_count($data['captcha_type'], '/') == 1) {
          list($module, $captcha_type) = explode('/', $data['captcha_type']);
        }
        else {
          $module = '';
          $captcha_type = $data['captcha_type'];
        }

        $point = $instances[$form_id];
        if ($point->export_type & EXPORT_IN_CODE && !($point->export_type & EXPORT_IN_DATABASE) && $point->module == $module && $point->captcha_type == $captcha_type) {
          continue;
        }
      }
      $data['captcha_settings'] = array();
      $data['captcha_settings']['size'] = $data['captcha_size'];
      $data['captcha_settings']['theme'] = $data['captcha_theme'];
      g_captcha_form_id_setting($form_id, $data['captcha_type'], $data['captcha_settings']);

    }

    // Add new CAPTCHA point?
    $captcha_point_form_id = $form_state['values']['captcha_form_protection']['captcha_form_id_overview']['captcha_new_captcha_point']['form_id'];
    if (!empty($captcha_point_form_id)) {
      $captcha_type = $form_state['values']['captcha_form_protection']['captcha_form_id_overview']['captcha_new_captcha_point']['captcha_type'];
      $captcha_settings = array(
        'captcha_size' => $form_state['values']['captcha_form_protection']['captcha_form_id_overview']['captcha_new_captcha_point']['captcha_size'],
        'captcha_theme' => $form_state['values']['captcha_form_protection']['captcha_form_id_overview']['captcha_new_captcha_point']['captcha_theme'],
      );
      g_captcha_form_id_setting($captcha_point_form_id, $captcha_type, $captcha_settings);
      drupal_set_message(t('Added CAPTCHA point.'), 'status');
    }

  }

  variable_set('g_captcha', $conf);

}

/**
 * Validation handler for captcha_admin_settings form.
 */
function g_captcha_forms_form_validate($form, $form_state) {
  $form_id = $form_state['values']['captcha_form_protection']['captcha_form_id_overview']['captcha_new_captcha_point']['form_id'];
  if (!preg_match('/^[a-z0-9_]*$/', $form_id)) {
    form_set_error('captcha_form_id_overview][captcha_new_captcha_point][form_id', t('Illegal form_id'));
  }
}

/**
 * Helper function for adding/updating a CAPTCHA point.
 *
 * @param string $form_id
 *   the form ID to configure.
 *
 * @param string $captcha_type
 *   the setting for the given form_id, can be:
 *   - 'none' to disable CAPTCHA,
 *   - 'default' to use the default challenge type
 *   - NULL to remove the entry for the CAPTCHA type
 *   - something of the form 'image_captcha/Image'
 *   - an object with attributes $captcha_type->module and $captcha_type->captcha_type
 */
function g_captcha_form_id_setting($form_id, $captcha_type, $captcha_settings) {
  db_merge('g_captcha_instances')
    ->key(array('form_id' => $form_id))
    ->fields(
        array(
          'captcha_type' => $captcha_type,
          'captcha_settings' => json_encode($captcha_settings)
        )
      )
    ->execute();
}
