<?php

/**
 * @file
 * G Captcha settings form and processing functions.
 */

/**
 * Create form for managing Google reCAPTCHA keys
 * and different settings
 *
 * @param $form
 * @param &$form_state
 *
 * @return array
 */
function g_captcha_settings_form($form, &$form_state) {
  $tune = variable_get('g_captcha');

  $form['api_keys'] = array(
    '#type' => 'fieldset',
    '#title' => t('API Keys'),
    '#tree' => FALSE,
    '#description' => t('These can be retrieved from the
      <a href="@google-recaptcha-site" target="_blank">Google reCAPTCHA site.
      </a>.', array(
        '@google-recaptcha-site' => 'https://www.google.com/recaptcha'
      )
    ),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE
  );
  $form['api_keys']['public_key'] = array(
    '#title' => t('Public Key'),
    '#type' => 'textfield',
    '#default_value' => empty($tune['public_key']) ? '' : $tune['public_key'],
    // may be without additional check?
    '#size' => 40,
    '#maxlength' => 40,
    '#required' => TRUE,
  );
  $form['api_keys']['secret_key'] = array(
    '#title' => t('Secret Key'),
    '#type' => 'textfield',
    '#default_value' => empty($tune['secret_key']) ? '' : $tune['secret_key'],
    '#size' => 40,
    '#maxlength' => 40,
    '#required' => TRUE,
  );
  $form['widget_display'] = array(
    '#type' => 'fieldset',
    '#title' => t('Widget Defaults'),
    '#tree' => FALSE,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );
  $form['widget_display']['widget_type'] = array(
    '#title' => t('Type'),
    '#type' => 'select',
    '#options' => array(
      'normal' => 'Invisible reCAPTCHA',
      'compact' => 'reCAPTCHA V2'
    ),
    '#default_value' => empty($tune['settings']['widget_size']) ? 'normal' : $tune['settings']['widget_size'],
  );
  $form['widget_display']['language'] = array(
    '#title' => t('Language'),
    '#description' => t('Enter the language code that can be found here: https://developers.google.com/recaptcha/docs/language'),
    '#type' => 'select',
    '#default_value' => empty($tune['settings']['language']) ? 'en' : $tune['settings']['language'],
    '#options' => g_captcha_language_code_options(),
  );
  $form['widget_display']['widget_size'] = array(
    '#title' => t('Size'),
    '#type' => 'select',
    '#options' => array(
      'normal' => 'Normal',
      'compact' => 'Compact'
    ),
    '#default_value' => empty($tune['settings']['widget_size']) ? 'normal' : $tune['settings']['widget_size'],
  );
  $form['widget_display']['widget_theme'] = array(
    '#title' => t('Theme'),
    '#type' => 'select',
    '#options' => array(
      'light' => 'Light',
      'dark' => 'Dark'
    ),
    '#default_value' => empty($tune['settings']['widget_theme']) ? 'dark' : $tune['settings']['widget_theme'],
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save')
  );

  return $form;
}

/**
 * Validate handler
 *
 * @param $form
 * @param $form_state
 */
function g_captcha_settings_form_validate($form, &$form_state) {
  if (strlen($form_state['values']['public_key']) != 40) {
    form_set_error('public_key', t('Length of public key must be 40 symbols long.'));
  }
  if (strlen($form_state['values']['secret_key']) != 40) {
    form_set_error('secret_key', t('Length of secret key must be 40 symbols long.'));
  }
}

/**
 * Submit handler
 *
 * @param $form
 * @param $form_state
 */
function g_captcha_settings_form_submit($form, &$form_state) {
  $conf = variable_get('g_captcha');
  $conf['public_key'] = $form_state['values']['public_key'];
  $conf['secret_key'] = $form_state['values']['secret_key'];
  $conf['settings']['widget_type'] = $form_state['values']['widget_type'];
  $conf['settings']['widget_size'] = $form_state['values']['widget_size'];
  $conf['settings']['widget_theme'] = $form_state['values']['widget_theme'];
  $conf['settings']['language'] = $form_state['values']['language'];

  variable_set('g_captcha', $conf);
  drupal_set_message(t('G Captcha settings were saved'));
}
